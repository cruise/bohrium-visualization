﻿<#@ template debug="false" hostspecific="false" language="C#" #>
<#@ output extension=".cs" #>
#region Copyright
/*
This file is part of Bohrium and copyright (c) 2012 the Bohrium
team <http://www.bh107.org>.

Bohrium is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 
of the License, or (at your option) any later version.

Bohrium is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the 
GNU Lesser General Public License along with Bohrium. 

If not, see <http://www.gnu.org/licenses/>.
*/
#endregion


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NumCIL.Generic;

namespace NumCIL.Unsafe
{
    internal static class Reduce
    {
<# foreach(string typename in new string[] {"SByte", "Byte", "Int16", "UInt16", "Int32", "UInt32", "Int64", "UInt64", "Single", "Double"}) { #>
        /// <summary>
        /// Unsafe implementation of the reduce operation
        /// </summary>
        /// <typeparam name="C">The type of operation to reduce with</typeparam>
        /// <param name="op">The instance of the operation to reduce with</param>
        /// <param name="in1">The input argument</param>
        /// <param name="axis">The axis to reduce</param>
        /// <param name="out">The output target</param>
        /// <returns>The output target</returns>
        private static NdArray<System.<#=typename#>> UFunc_Reduce_Inner_Flush_<#=typename#><C>(C op, long axis, NdArray<System.<#=typename#>> in1, NdArray<System.<#=typename#>> @out)
            where C : struct, IBinaryOp<System.<#=typename#>>
        {
            if (axis < 0)
                axis = in1.Shape.Dimensions.LongLength - axis;

            //Basic case, just return a reduced array
            if (in1.Shape.Dimensions[axis].Length == 1 && in1.Shape.Dimensions.LongLength > 1)
            {
                //TODO: If both in and out use the same array, just return a reshaped in
                long j = 0;
                var sizes = in1.Shape.Dimensions.Where(x => j++ != axis).ToArray();
                Apply.UFunc_Op_Inner_Unary_Flush_<#=typename#><CopyOp<System.<#=typename#>>>(new CopyOp<System.<#=typename#>>(), in1.Reshape(new Shape(sizes, in1.Shape.Offset)), ref @out);
            }
            else
            {
                unsafe
                {
                    using (var f1 = new Pinner(in1.DataAccessor))
                    using (var f2 = new Pinner(@out.DataAccessor))
                    {
                        var d = (System.<#=typename#>*)f1.ptr;
                        var vd = (System.<#=typename#>*)f2.ptr;

                        //Simple case, reduce 1D array to scalar value
                        if (axis == 0 && in1.Shape.Dimensions.LongLength == 1)
                        {
                            long stride = in1.Shape.Dimensions[0].Stride;
                            long ix = in1.Shape.Offset;
                            long limit = (stride * in1.Shape.Dimensions[0].Length) + ix;

                            System.<#=typename#> value = d[ix];

                            for (long i = ix + stride; i < limit; i += stride)
                                value = op.Op(value, d[i]);

                            vd[@out.Shape.Offset] = value;
                        }
                        //Simple case, reduce 2D array to 1D
                        else if (axis == 0 && in1.Shape.Dimensions.LongLength == 2)
                        {
                            long strideInner = in1.Shape.Dimensions[1].Stride;
                            long strideOuter = in1.Shape.Dimensions[0].Stride;

                            long ix = in1.Shape.Offset;
                            long ox = @out.Shape.Offset;
                            long strideRes = @out.Shape.Dimensions[0].Stride;

                            long outerCount = in1.Shape.Dimensions[0].Length;

                            for (long i = 0; i < in1.Shape.Dimensions[1].Length; i++)
                            {
                                System.<#=typename#> value = d[ix];

                                long nx = ix;
                                for (long j = 1; j < outerCount; j++)
                                {
                                    nx += strideOuter;
                                    value = op.Op(value, d[nx]);
                                }

                                vd[ox] = value;
                                ox += strideRes;

                                ix += strideInner;
                            }
                        }
                        //Simple case, reduce 2D array to 1D
                        else if (axis == 1 && in1.Shape.Dimensions.LongLength == 2)
                        {
                            long strideInner = in1.Shape.Dimensions[1].Stride;
                            long strideOuter = in1.Shape.Dimensions[0].Stride;

                            long ix = in1.Shape.Offset;
                            long limitInner = strideInner * in1.Shape.Dimensions[1].Length;

                            long ox = @out.Shape.Offset;
                            long strideRes = @out.Shape.Dimensions[0].Stride;

                            for (long i = 0; i < in1.Shape.Dimensions[0].Length; i++)
                            {
                                System.<#=typename#> value = d[ix];

                                for (long j = strideInner; j < limitInner; j += strideInner)
                                    value = op.Op(value, d[j + ix]);

                                vd[ox] = value;
                                ox += strideRes;

                                ix += strideOuter;
                            }
                        }
                        //General case
                        else
                        {
                            long size = in1.Shape.Dimensions[axis].Length;
                            NdArray<System.<#=typename#>> vl = @out.Subview(Range.NewAxis, axis);

                            //Initially we just copy the value
                            Apply.UFunc_Op_Inner_Unary_Flush_<#=typename#><CopyOp<System.<#=typename#>>>(new CopyOp<System.<#=typename#>>(), in1.Subview(Range.El(0), axis), ref vl);

                            //If there is more than one element in the dimension to reduce, apply the operation accumulatively
                            for (long j = 1; j < size; j++)
                            {
                                //Select the new dimension
                                //Apply the operation
                                Apply.UFunc_Op_Inner_Binary_Flush_<#=typename#><C>(op, vl, in1.Subview(Range.El(j), axis), ref vl);
                            }
                        }
                    }
                }
            }
            return @out;
        }
<# } #>
	}
}
