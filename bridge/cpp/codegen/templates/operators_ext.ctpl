#slurp
#compiler-settings
directiveStartToken = %
#end compiler-settings
%slurp
//
//  Binary operators such as:
//  Mapping "a + b" to BH_ADD(t, a, b)
//  Mapping "a + 1.0" to BH_ADD(t, a, 1.0)
//  Mapping "1.0 + a" to BH_ADD(t, 1.0, a)
//
%for $op, $opcode, $optype, $opcount, $typesigs in $data
%if $optype == "operator.ext" and $opcode != "CUSTOM" and $opcount == 3
%for $ret_type, $op1_type, $op2_type in $typesigs

multi_array<$ret_type>& operator$op (multi_array<$op1_type>& lhs, multi_array<$op2_type>& rhs)
{
    multi_array<$ret_type>* result  = &Runtime::instance().temp<$ret_type>(); 
    multi_array<$op1_type>* left    = &lhs;
    multi_array<$op2_type>* right   = &rhs;

    if (same_shape(lhs, rhs)) {
        equiv<$ret_type, $op1_type>(*result, lhs);
    } else {

        if (lhs.getRank() < rhs.getRank()) {    // Left-handside has lowest rank
            left    = &Runtime::instance().temp_view(lhs);
            right   = &rhs;
            if (!broadcast(lhs, rhs, *left)) {
                throw std::runtime_error("Failed broadcasting.");
            }
            equiv<$ret_type, $op1_type>(*result, *left);

        } else {                                // Right-handside has lowest rank
            left    = &lhs;
            right   = &Runtime::instance().temp_view(rhs);
            if (!broadcast(rhs, lhs, *right)) {
                throw std::runtime_error("Failed broadcasting.");
            }
            equiv<$ret_type, $op1_type>(*result, *right);
        }
    }
    Runtime::instance().enqueue((bh_opcode)$opcode, *result, *left, *right);
    return *result;
}

multi_array<$ret_type> & operator$op (multi_array<$op1_type>& lhs, const $op2_type& rhs)
{
    multi_array<$ret_type>* result = &Runtime::instance().temp<$ret_type>(); 
    equiv<$ret_type, $op1_type>(*result, lhs);

    Runtime::instance().enqueue((bh_opcode)$opcode, *result, lhs, rhs);

    return *result;
}

multi_array<$ret_type> & operator$op (const $op1_type& lhs, multi_array<$op2_type>& rhs)
{
    multi_array<$ret_type>* result = &Runtime::instance().temp<$ret_type>(); 
    equiv<$ret_type, $op1_type>(*result, rhs);

    Runtime::instance().enqueue((bh_opcode)$opcode, *result, lhs, rhs);

    return *result;
}
%slurp
%end for
%end if
%slurp
%end for

//
//  Unary operators such as:
//  Mapping "!a" to BH_NEGATE(t, a)
//
%for $op, $opcode, $optype, $opcount, $typesigs in $data
%if $optype == "operator.ext" and $opcode != "CUSTOM" and $opcount == 2

template <typename T>
multi_array<T> & operator$op (multi_array<T>& rhs)
{
    multi_array<T>* result = &Runtime::instance().temp(rhs);

    Runtime::instance().enqueue((bh_opcode)$opcode, *result, rhs);

    return *result;
}
%slurp
%end if
%end for

