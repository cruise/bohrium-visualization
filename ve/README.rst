===================================
Brief Description of Vector Engines
===================================

Here goes::

    gpu      - GPU vector engine.
    cpu      - CPU vector engine.
    tiling   - CPU vector engine with tiling.
    mcore    - CPU vector engine with TLP.
    dynamite - JIT compiled engine for CPU, the first of its kind ;)
    print    - prints the bytecode stream

