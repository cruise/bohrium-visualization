#slurp
#compiler-settings
directiveStartToken = %
#end compiler-settings
%slurp
/*
This file is part of Bohrium and copyright (c) 2012 the Bohrium
team <http://www.bh107.org>.

Bohrium is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as 
published by the Free Software Foundation, either version 3 
of the License, or (at your option) any later version.

Bohrium is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the 
GNU Lesser General Public License along with Bohrium. 

If not, see <http://www.gnu.org/licenses/>.
*/
#include <bh.h>

/* Determines if the types are acceptable for the operation
 *
 * @opcode Opcode for operation
 * @outtype The type of the output
 * @inputtype1 The type of the first input operand
 * @inputtype2 The type of the second input operand
 * @constanttype The type of the constant
 * @return TRUE if the types are accepted, FALSE otherwise
 */
bool bh_validate_types(bh_opcode opcode, bh_type outtype, bh_type inputtype1, bh_type inputtype2, bh_type constanttype)
{
    // Poly contains a unique value, pairing an opcode with its function signature.
    // All in one nicely switchable value.
    long int poly;
 
    if (bh_operands(opcode) == 3) {                 // Three operands

        if (inputtype1 == BH_UNKNOWN) {             // First operand is constant
            poly = opcode \
                | (outtype << 8) \
                | (constanttype << 12) \
                | (inputtype2 << 16);

        } else if (inputtype2 == BH_UNKNOWN) {      // Second operand is constant
            poly = opcode
                | (outtype << 8) \
                | (inputtype1 << 12) \
                | (constanttype << 16);

        } else {                                     // No constant operand
            poly = opcode \
                | (outtype << 8) \
                | (inputtype1 << 12) \
                | (inputtype2 << 16);
        }

    } else {                                         // Two operands

        if (inputtype1 == BH_UNKNOWN) {
            poly = opcode \
                | (outtype << 8) \
                | (constanttype << 12) \
                | (1 << 17);
        } else {
            poly = opcode \
                | (outtype << 8) \
                | (inputtype1 << 12) \
                | (1 << 17);
        }
    }
    

    switch (poly)
    {
        %for $case in $data
        %if $case.nop == 2
        case ${case.opcode} | (${case.op1} << 8) | (${case.op2} << 12) | (1 << 17):
        %else
        case ${case.opcode} | (${case.op1} << 8) | (${case.op2} << 12) | (${case.op3} << 16):
        %end if
        %end for
            return true;
                    
        default:
            return false;
    }

}

/* Determines if the types are acceptable for the operation, 
 * and provides a suggestion for converting them
 *
 * @opcode Opcode for operation
 * @outtype The type of the output
 * @inputtype1 The type of the first input operand
 * @inputtype2 The type of the second input operand
 * @constanttype The type of the constant
 * @return TRUE if the types can be converted, FALSE otherwise
 */
bool bh_get_type_conversion(bh_opcode opcode, bh_type outtype, bh_type* inputtype1, bh_type* inputtype2, bh_type* constanttype) 
{
    // Basic case, "it just works"
    if (bh_validate_types(opcode, outtype, *inputtype1, *inputtype2, *constanttype))
        return true;
        
    // All valid identity types are covered
    if (opcode == BH_IDENTITY)
        return false;
    
    // Grab the input types
    bh_type desired_input_type1 = BH_UNKNOWN;
    bh_type desired_input_type2 = BH_UNKNOWN;

    // Poly contains a unique value, pairing an opcode with its function signature.
    // All in one nicely switchable value.
    long int poly;
    
    poly = opcode | (outtype << 8);

    switch(poly)
    {
		%set $lastoutputtype = ''
        %for $case in $data
		%if $case.op1 != $lastoutputtype
        %if $case.opcode != 'BH_IDENTITY'
            case ${case.opcode} | (${case.op1} << 8):
                desired_input_type1 = ${case.op2};
                %if $case.nop == 3
                desired_input_type2 = ${case.op3};
                %end if
                break;
        %end if
        %end if
        %set $lastoutputtype = $case.op1
        %end for
    }
    
    // The output type does not exist for the opcode
    if (desired_input_type1 == BH_UNKNOWN)
        return false;
    
    if (bh_operands(opcode) == 3)
    {
        // The output type does not exist for the opcode
        if (desired_input_type2 == BH_UNKNOWN)
            return false;

        if (*inputtype1 == BH_UNKNOWN)                      // First operand constant
        {
        // Check if we can convert with IDENTITY
            if (bh_validate_types(BH_IDENTITY, desired_input_type1, *constanttype, BH_UNKNOWN, BH_UNKNOWN) && bh_validate_types(BH_IDENTITY, desired_input_type2, *inputtype2, BH_UNKNOWN, BH_UNKNOWN))
            {
                *constanttype = desired_input_type1;
                *inputtype2 = desired_input_type2;
                return true;
            }
        }
        else if (*inputtype2 == BH_UNKNOWN)                 // Second operand constant
        {
            // Check if we can convert with IDENTITY
            if (bh_validate_types(BH_IDENTITY, desired_input_type1, *inputtype1, BH_UNKNOWN, BH_UNKNOWN) && bh_validate_types(BH_IDENTITY, desired_input_type2, *constanttype, BH_UNKNOWN, BH_UNKNOWN))
            {
                *inputtype1 = desired_input_type1;
                *constanttype = desired_input_type2;
                return true;
            }
        }
        else                                                // No constant
        {
            // Check if we can convert with IDENTITY
            if (bh_validate_types(BH_IDENTITY, desired_input_type1, *inputtype1, BH_UNKNOWN, BH_UNKNOWN) && bh_validate_types(BH_IDENTITY, desired_input_type2, *inputtype2, BH_UNKNOWN, BH_UNKNOWN))
            {
                *inputtype1 = desired_input_type1;
                *inputtype2 = desired_input_type2;
                return true;
            }
        }
    }
    else
    {
        // Check if we can convert with IDENTITY
        if (*inputtype1 == BH_UNKNOWN)                      // Constant input
        {
            if (bh_validate_types(BH_IDENTITY, desired_input_type1, *constanttype, BH_UNKNOWN, BH_UNKNOWN))
            {
                *constanttype = desired_input_type1;
                return true;
            }
        }
        else                                                // No constant
        {
            if (bh_validate_types(BH_IDENTITY, desired_input_type1, *inputtype1, BH_UNKNOWN, BH_UNKNOWN))
            {
                *inputtype1 = desired_input_type1;
                return true;
            }
        }
    }
    
    // Not possible to convert the types automatically
    return false;
}