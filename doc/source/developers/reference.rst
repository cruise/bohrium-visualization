.. _developer_reference:

API Reference
=============

Bla bla bla

Core
----

.. doxygenstruct:: bh_array
   :project: Bohrium

.. doxygenfunction:: bh_create_array
   :project: Bohrium

.. doxygenfunction:: bh_destroy_array
   :project: Bohrium


Utilities
~~~~~~~~~

...

Printing
::::::::

.. doxygensummary:: bh_pprint.h
   :project: Bohrium

Bridges / Language Frontends
----------------------------

numpy / CIL / C++


Vector Engine Managers
----------------------

node / cluster

Vector Engines
--------------

simple / score / mcore /gpu


